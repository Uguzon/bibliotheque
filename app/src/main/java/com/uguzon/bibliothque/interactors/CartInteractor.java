package com.uguzon.bibliothque.interactors;

import android.arch.lifecycle.LiveData;

import com.uguzon.bibliothque.views.models.book.Book;

import java.util.List;

public interface CartInteractor {
    LiveData<List<Book>> getCart();

    void addToCart(Book book);

    void removeFromCart(Book book);

    void clearCart();





}
