package com.uguzon.bibliothque.interactors;

import android.arch.lifecycle.MutableLiveData;

import com.uguzon.bibliothque.views.models.book.Book;

import java.util.List;

public interface LibraryContentInteractor {

    MutableLiveData<List<Book>> getLibraryBookList();

}
