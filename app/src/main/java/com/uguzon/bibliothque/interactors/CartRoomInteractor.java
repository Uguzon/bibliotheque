package com.uguzon.bibliothque.interactors;

import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.uguzon.bibliothque.repositories.room_database.AppDatabase;
import com.uguzon.bibliothque.views.models.book.Book;

import java.util.List;

public class CartRoomInteractor implements CartInteractor {

    private final AppDatabase appDatabase;

    public CartRoomInteractor(AppDatabase appDatabase) {
        this.appDatabase = appDatabase;
    }

    public LiveData<List<Book>> getCart() {
        return appDatabase.bookDao().getBooks();
    }

    public void addToCart(Book book) {
        new addAsyncTask(appDatabase).execute(book);
    }

    public void removeFromCart(Book book) {
        new deleteAsyncTask(appDatabase).execute(book);
    }

    public void clearCart() {
        new clearCartAsyncTask(appDatabase).execute();
    }

    private static class deleteAsyncTask extends AsyncTask<Book, Void, Void> {

        private AppDatabase db;

        deleteAsyncTask(AppDatabase appDatabase) {
            db = appDatabase;
        }

        @Override
        protected Void doInBackground(final Book... params) {
            db.bookDao().deleteBook(params[0]);
            return null;
        }
    }

    private static class addAsyncTask extends AsyncTask<Book, Void, Void> {

        private AppDatabase db;

        addAsyncTask(AppDatabase appDatabase) {
            db = appDatabase;
        }

        @Override
        protected Void doInBackground(final Book... params) {
            db.bookDao().insertBook(params[0]);
            return null;
        }
    }

    private static class clearCartAsyncTask extends AsyncTask<Book, Void, Void> {

        private AppDatabase db;

        clearCartAsyncTask(AppDatabase appDatabase) {
            db = appDatabase;
        }

        @Override
        protected Void doInBackground(final Book... params) {
            db.bookDao().clearAllBooks();
            return null;
        }
    }

}
