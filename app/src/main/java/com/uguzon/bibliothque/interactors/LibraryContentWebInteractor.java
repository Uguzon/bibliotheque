package com.uguzon.bibliothque.interactors;

import android.app.Activity;
import android.arch.lifecycle.MutableLiveData;
import android.net.Uri;
import android.util.Log;

import com.android.volley.Response;
import com.uguzon.bibliothque.repositories.WebJSONRepository;
import com.uguzon.bibliothque.views.models.book.Book;
import com.uguzon.bibliothque.views.models.book.mappers.BookJsonMapper;

import org.json.JSONArray;

import java.util.List;

public class LibraryContentWebInteractor implements LibraryContentInteractor {

    private static final String TAG = LibraryContentWebInteractor.class.getSimpleName();
    private WebJSONRepository repo;

    public LibraryContentWebInteractor(Activity activity) {
        repo = new WebJSONRepository(activity);
    }

    @Override
    public MutableLiveData<List<Book>> getLibraryBookList() {

        final MutableLiveData<List<Book>> bookList = new MutableLiveData<>();

        Uri.Builder builder = repo.getBooksBuilder();

        repo.getJSONArray(builder.build().toString(), new Response.Listener<JSONArray>() {
            @Override
            public void onResponse(JSONArray response) {
                Log.v(TAG, "Got response : " + response.toString());
                bookList.setValue(new BookJsonMapper().createBookList(response));
            }
        });
        return bookList;
    }

}
