package com.uguzon.bibliothque.interactors;

import android.app.Activity;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.net.Uri;
import android.util.Log;

import com.android.volley.Response;
import com.uguzon.bibliothque.repositories.WebJSONRepository;
import com.uguzon.bibliothque.views.models.book.Book;
import com.uguzon.bibliothque.views.models.offer.Offer;
import com.uguzon.bibliothque.views.models.offer.mappers.OfferJsonMapper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class OffersWebInteractor implements OffersInteractor {

    private static final String TAG = OffersWebInteractor.class.getSimpleName();
    private static final String OFFERS_PROPERTY = "offers";
    private List<Book> mBookList;
    private WebJSONRepository repo;


    public OffersWebInteractor(Activity activity) {
        repo = new WebJSONRepository(activity);
    }

    @Override
    public LiveData<List<Offer>> getOffers(List<Book> bookList) {
        mBookList = bookList;
        final MutableLiveData<List<Offer>> offerList = new MutableLiveData<>();

        if (mBookList != null && !mBookList.isEmpty()) {
            Uri.Builder builder = repo.getBooksBuilder();
            getUrlDataDiscountOffers(mBookList, builder);
            repo.getJSONObject(builder.build().toString(), new Response.Listener<JSONObject>() {
                @Override
                public void onResponse(JSONObject response) {
                    Log.v(TAG, "Got JSONObject response : " + response.toString());
                    try {
                        JSONArray jsonArray = response.getJSONArray(OFFERS_PROPERTY);
                        offerList.setValue(new OfferJsonMapper().createOfferList(mBookList, jsonArray));
                    } catch (JSONException e) {
                        offerList.setValue(new ArrayList<Offer>());
                        e.printStackTrace();
                    }

                }
            });
        }
        return offerList;
    }

    private static void getUrlDataDiscountOffers(List<Book> books, Uri.Builder builder) {
        if (books != null && books.size() > 0) {
            StringBuilder isbnList = new StringBuilder(books.get(0).getIsbn());
            for (int i=1 ; i < books.size() ; i++ ) {
                isbnList.append("," + books.get(i).getIsbn());
            }
            builder.appendPath(isbnList.toString());
            builder.appendPath(WebJSONRepository.URI_COMMERCIAL_OFFERS_PATH);
        }
    }


}
