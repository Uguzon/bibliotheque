package com.uguzon.bibliothque.interactors;

import android.arch.lifecycle.LiveData;

import com.uguzon.bibliothque.views.models.book.Book;
import com.uguzon.bibliothque.views.models.offer.Offer;

import java.util.List;

public interface OffersInteractor {

    LiveData<List<Offer>> getOffers(List<Book> bookList);

}
