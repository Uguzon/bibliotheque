package com.uguzon.bibliothque.viewModels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.uguzon.bibliothque.interactors.CartInteractor;
import com.uguzon.bibliothque.interactors.OffersInteractor;
import com.uguzon.bibliothque.views.models.book.Book;
import com.uguzon.bibliothque.views.models.offer.Offer;

import java.util.List;

public class CartViewModel extends ViewModel {

    private LiveData<List<Book>> cartList;
    private LiveData<List<Offer>> discountList;

    public void init(CartInteractor cartInteractor) {
        cartList = cartInteractor.getCart();
        discountList = new MutableLiveData<>();
    }

    public void initDiscount(OffersInteractor offersInteractor, List<Book> bookList) {
        discountList = offersInteractor.getOffers(bookList);
    }


    public LiveData<List<Book>> getCartList() {
        return cartList;
    }

    public LiveData<List<Offer>> getOfferList() { return discountList; }

}

