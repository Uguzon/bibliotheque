package com.uguzon.bibliothque.viewModels;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.arch.lifecycle.ViewModel;

import com.uguzon.bibliothque.views.models.book.Book;
import com.uguzon.bibliothque.interactors.CartInteractor;
import com.uguzon.bibliothque.interactors.LibraryContentInteractor;

import java.util.List;

public class LibraryViewModel extends ViewModel {

    private LiveData<List<Book>> cartList;
    private MutableLiveData<List<Book>> bookList;

    public void init(CartInteractor cartInteractor, LibraryContentInteractor libraryContentInteractor) {
        cartList = cartInteractor.getCart();
        bookList = libraryContentInteractor.getLibraryBookList();
    }


    public  LiveData<List<Book>> getBookList() {
        return bookList;
    }

    public LiveData<List<Book>> getCartList() {
        return cartList;
    }

}
