package com.uguzon.bibliothque.repositories.room_database;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.arch.persistence.room.TypeConverters;
import android.content.Context;

import com.uguzon.bibliothque.views.models.book.Book;

@Database(entities = {Book.class}, version = 1)
@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract BookDao bookDao();

    private static AppDatabase INSTANCE;
    private static final String DATABASE_NAME = "books_database";


    public static AppDatabase getDataBaseInstance(Context context) {
        if (INSTANCE == null) {
            INSTANCE =  Room.databaseBuilder(
                    context.getApplicationContext(),
                    AppDatabase.class,
                    DATABASE_NAME).build();
        }
        return INSTANCE;
    }
}

