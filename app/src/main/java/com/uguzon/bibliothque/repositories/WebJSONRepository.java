package com.uguzon.bibliothque.repositories;

import android.app.Activity;
import android.net.Uri;
import android.support.annotation.Nullable;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonArrayRequest;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONObject;

import java.net.URL;

public class WebJSONRepository {

    private static final String TAG = WebJSONRepository.class.getSimpleName();

    public static final String URI_SCHEME = "http";
    public static final String URI_AUTHORITY = "henri-potier.xebia.fr";
    public static final String URI_BOOKS_PATH = "books";
    public static final String URI_COMMERCIAL_OFFERS_PATH = "commercialOffers";

    private Activity mActivity;

    public WebJSONRepository(Activity activity) {
        mActivity = activity;
    }

    public void getJSONArray(@Nullable String urlData, Response.Listener<JSONArray> listener) {

            RequestQueue queue = Volley.newRequestQueue(mActivity);

            JsonArrayRequest jsonArrayRequest = new JsonArrayRequest(Request.Method.GET, urlData, null,
                    listener,
                    new Response.ErrorListener() {
                        @Override
                        public void onErrorResponse(VolleyError error) {
                            error.printStackTrace();
                        }
                    });

            queue.add(jsonArrayRequest);

    }

    public void getJSONObject(@Nullable String urlData, Response.Listener<JSONObject> listener) {

        RequestQueue queue = Volley.newRequestQueue(mActivity);

        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.GET, urlData, null,
                listener,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        error.printStackTrace();
                    }
                });

        queue.add(jsonObjectRequest);
    }

    public Uri.Builder getBooksBuilder() {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme(URI_SCHEME)
                .authority(URI_AUTHORITY)
                .appendPath(URI_BOOKS_PATH);
        return builder;
    }
}
