package com.uguzon.bibliothque.repositories.room_database;

import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;

import com.uguzon.bibliothque.views.models.book.Book;

import java.util.List;

@Dao
public interface BookDao {
    @Query("SELECT * FROM book")
    LiveData<List<Book>> getBooks();

    @Query("SELECT * FROM book where isbn = :isbn")
    Book find(String isbn);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertBook(Book user);

    @Delete
    void deleteBook(Book user);

    @Query("DELETE FROM book")
    void clearAllBooks();


}
