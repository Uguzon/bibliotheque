package com.uguzon.bibliothque.util;

import android.content.Context;

import com.uguzon.bibliothque.R;
import com.uguzon.bibliothque.views.models.book.Book;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class Util {

    /**
     * Scale used for converting Double values into BigDecimal
     */
    private static final int SCALE = 2;

    /**
     * Rounding mode used for converting Double values into BigDecimal
     */
    private static final RoundingMode ROUNDING_MODE = RoundingMode.HALF_EVEN;


    public static String getStringPrice(Context context, int price) {
        return getStringPrice(context, getBigDecimal(price));
    }

    public static String getStringPrice(Context context, double price) {
        String priceBd = getBigDecimal(price).toString();
        return context.getString(R.string.price, priceBd);
    }

    public static String getStringPrice(Context context, BigDecimal price) {
        return context.getString(R.string.price, price.toString());
    }

    public static String getStringPrice(Context context, Book book) {
        return getStringPrice(context, book.getPrice());
    }

    public static BigDecimal getBigDecimal(Double doubleValue) {
        return new BigDecimal(doubleValue).setScale(SCALE,  ROUNDING_MODE);
    }

    public static BigDecimal getBigDecimal(int intValue) {
        return new BigDecimal(intValue).setScale(SCALE, ROUNDING_MODE);
    }

    public static BigDecimal getBigDecimal(BigDecimal bigDecimal) {
        return bigDecimal.setScale(SCALE, ROUNDING_MODE);
    }

}
