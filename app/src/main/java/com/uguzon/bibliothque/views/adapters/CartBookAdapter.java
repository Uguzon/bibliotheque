package com.uguzon.bibliothque.views.adapters;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.uguzon.bibliothque.R;
import com.uguzon.bibliothque.views.models.book.Book;
import com.uguzon.bibliothque.views.fragments.CartBookDialogFragment;

import butterknife.BindView;
import butterknife.ButterKnife;

public class CartBookAdapter extends BookAdapter<CartBookAdapter.BookAdapterViewHolder> {

    private static final String TAG = CartBookAdapter.class.getSimpleName();

    public CartBookAdapter(AppCompatActivity activity) {
            super(activity);
    }

    @NonNull
    @Override
    public BookAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.book_item_cart, parent, false);
        return new BookAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BookAdapterViewHolder holder, int position) {
        holder.bind(position);
    }

    public class BookAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.iv_cover) ImageView mCoverView;
        @BindView(R.id.tv_title) TextView mTitleView;
        @BindView(R.id.tv_price) TextView mPriceView;

        int myPosition;

        BookAdapterViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);

            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            showBookDialog(mBooks.get(myPosition));
        }

        private void bind(int position) {
            myPosition = position;

            final Book book = mBooks.get(position);
            Picasso.with(mContext)
                    .load(book.getCoverUrl())
                    .into(mCoverView);
            mTitleView.setText(book.getTitle());
            mPriceView.setText(book.getStringPrice(mContext));
        }
    }

    private void showBookDialog(final Book book) {
        CartBookDialogFragment bookDialog = CartBookDialogFragment.createInstance(book);
        bookDialog.show(mActivity.getSupportFragmentManager(), TAG);
    }
}
