package com.uguzon.bibliothque.views.adapters;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.uguzon.bibliothque.views.models.book.Book;
import com.uguzon.bibliothque.R;
import com.uguzon.bibliothque.views.fragments.LibraryBookDialogFragment;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LibraryBookAdapter extends BookAdapter<LibraryBookAdapter.BookAdapterViewHolder> {

    private static final String TAG = LibraryBookAdapter.class.getSimpleName();
    private int mExpandedPosition = -1;
    private int mPreviousExpandedPosition = -1;
    private List<Book> mCartList;

    public LibraryBookAdapter(AppCompatActivity activity) {
        super(activity);
    }

    @NonNull
    @Override
    public BookAdapterViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        mContext = parent.getContext();
        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = inflater.inflate(R.layout.book_item_library, parent, false);
        return new BookAdapterViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull BookAdapterViewHolder holder, int position) {
        holder.bind(position);
    }

    public void setCartList(List<Book> cartList) {
        mCartList = cartList;
        Log.v(TAG, "CartList has been set");
        notifyDataSetChanged();
    }

    public class BookAdapterViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.iv_cover) ImageView mCoverView;
        @BindView(R.id.tv_title) TextView mTitleView;
        @BindView(R.id.tv_price) TextView mPriceView;
        @BindView(R.id.tv_short_synopsis) TextView mShortSynopsisView;
        @BindView(R.id.tv_add_to_cart) TextView mAddToCartView;
        @BindView(R.id.tv_in_cart_already) TextView mAlreadyInCartView;
        @BindView(R.id.tv_long_synopsis) TextView mLongSynopsisView;
        @BindView(R.id.separator_unexpanded) View mSeparatorUnexpanded;
        @BindView(R.id.separator_expanded) View mSeparatorExpanded;

        boolean isExpanded;
        int myPosition;

        BookAdapterViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            Log.v(TAG, "cliqué : " + myPosition);
            mExpandedPosition = isExpanded ? -1: myPosition;
            notifyItemChanged(mPreviousExpandedPosition);
            notifyItemChanged(myPosition);
        }

        private void bind(int position) {
            myPosition = position;
            isExpanded = myPosition == mExpandedPosition;
            itemView.setActivated(isExpanded);
            if (isExpanded) mPreviousExpandedPosition = myPosition;

            final Book book = mBooks.get(position);
            Picasso.with(mContext)
                    .load(book.getCoverUrl())
                    .into(mCoverView);
            mTitleView.setText(book.getTitle());
            mPriceView.setText(book.getStringPrice(mContext));
            mShortSynopsisView.setText(book.getLongSynopsis());
            mLongSynopsisView.setText(book.getLongSynopsis());

            mShortSynopsisView.setVisibility(isExpanded ? View.GONE : View.VISIBLE);
            mSeparatorUnexpanded.setVisibility(isExpanded ? View.GONE : View.VISIBLE);
            mLongSynopsisView.setVisibility(isExpanded ? View.VISIBLE : View.GONE);
            mSeparatorExpanded.setVisibility(isExpanded ? View.VISIBLE : View.GONE);

            if (isExpanded) {
                if (mCartList != null && Book.inList(mCartList, book)) {
                    mAddToCartView.setVisibility(View.GONE);
                    mAlreadyInCartView.setVisibility(View.VISIBLE);
                } else {
                    mAddToCartView.setVisibility(View.VISIBLE);
                    mAlreadyInCartView.setVisibility(View.GONE);
                }
            } else {
                mAddToCartView.setVisibility(View.GONE);
                mAlreadyInCartView.setVisibility(View.GONE);
            }

            mAddToCartView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mCartInteractor.addToCart(book);
                    showBookDialog(book);
                }
            });

        }
    }

    private void showBookDialog(Book book) {
        LibraryBookDialogFragment bookDialog = LibraryBookDialogFragment.createInstance(book);
        bookDialog.show(mActivity.getSupportFragmentManager(), TAG);
    }
}
