package com.uguzon.bibliothque.views.activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import com.uguzon.bibliothque.R;
import com.uguzon.bibliothque.interactors.CartInteractor;
import com.uguzon.bibliothque.interactors.OffersWebInteractor;
import com.uguzon.bibliothque.interactors.OffersInteractor;
import com.uguzon.bibliothque.views.adapters.CartBookAdapter;
import com.uguzon.bibliothque.repositories.room_database.AppDatabase;
import com.uguzon.bibliothque.views.fragments.CartBookDialogFragment;
import com.uguzon.bibliothque.interactors.CartRoomInteractor;
import com.uguzon.bibliothque.util.Util;
import com.uguzon.bibliothque.views.models.book.Book;
import com.uguzon.bibliothque.viewModels.CartViewModel;
import com.uguzon.bibliothque.views.models.book.BookHelper;
import com.uguzon.bibliothque.views.models.offer.Offer;
import com.uguzon.bibliothque.views.models.offer.OfferHelper;

import java.util.List;

public class CartActivity extends AppCompatActivity implements CartBookDialogFragment.CartBookDialogListener {

    private static final String TAG = LibraryActivity.class.getSimpleName();

    @BindView(R.id.tv_total_value) TextView mTotalView;
    @BindView(R.id.tv_total_with_discount_value) TextView mTotalWithDiscountView;
    @BindView(R.id.pb_loading_indicator) ProgressBar mLoadingIndicator;
    @BindView(R.id.tv_message_empty_book_list) TextView mBookListEmptyMessage;
    @BindView(R.id.scrollview) View mMainView;
    @BindView(R.id.recyclerview_books) RecyclerView mRecyclerView;

    private CartBookAdapter mBookAdapter;
    private CartViewModel viewModel;
    private CartInteractor cartInteractor;
    private OffersInteractor offersInteractor;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);
        ButterKnife.bind(this);

        cartInteractor = new CartRoomInteractor(AppDatabase.getDataBaseInstance(this));
        offersInteractor = new OffersWebInteractor(this);

        viewModel = ViewModelProviders.of(this).get(CartViewModel.class);
        viewModel.init(new CartRoomInteractor(AppDatabase.getDataBaseInstance(this)));
        viewModel.getCartList().observe(this, new Observer<List<Book>>() {
            @Override
            public void onChanged(@Nullable List<Book> books) {
                mBookAdapter.setBooks(books);
                setPrices(books);
                if (books == null || books.isEmpty()) {
                    showBookListEmptyMessage();
                } else {
                    showMainView();
                }
            }
        });

        mBookAdapter = new CartBookAdapter(this);

        mRecyclerView.setAdapter(mBookAdapter);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(layoutManager);

        showLoadingIndicator();

    }

    @OnClick(R.id.btn_clear_cart)
    public void clearCart() {
        cartInteractor.clearCart();
    }

    @OnClick(R.id.btn_action2)
    public void buyCart() {
        Toast.makeText(CartActivity.this, "C'est dans la poche", Toast.LENGTH_LONG).show();
        cartInteractor.clearCart();
        CartActivity.this.recreate();
    }

    private void setPrices(List<Book> cartList) {
        mTotalView.setText(Util.getStringPrice(this, BookHelper.getTotalPrice(cartList)));
        mTotalWithDiscountView.setText(mTotalView.getText().toString());
        if (cartList != null && !cartList.isEmpty()) {
            viewModel.initDiscount(offersInteractor, cartList);
            viewModel.getOfferList().observe(this, new Observer<List<Offer>>() {
                @Override
                public void onChanged(@Nullable List<Offer> offerList) {
                    if (offerList != null && !offerList.isEmpty()) {
                        Offer bestOffer = OfferHelper.getBestOffer(offerList);
                        mTotalWithDiscountView.setText(Util.getStringPrice(CartActivity.this, bestOffer.getTotalPriceWithDiscount()));
                    }
                }
            });
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        for (int i = 0; i < menu.size() ; i++) {
            if (menu.getItem(i).getItemId() != R.id.action_list) {
                menu.getItem(i).setVisible(false);
            }
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_list) {
            startActivity(new Intent(this, LibraryActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onRemoveFromCart(CartBookDialogFragment dialog, Book book) {
        cartInteractor.removeFromCart(book);
    }

    private void showLoadingIndicator() {
        mMainView.setVisibility(View.GONE);
        mLoadingIndicator.setVisibility(View.VISIBLE);
        mBookListEmptyMessage.setVisibility(View.GONE);
    }

    private void showBookListEmptyMessage() {
        mMainView.setVisibility(View.GONE);
        mLoadingIndicator.setVisibility(View.GONE);
        mBookListEmptyMessage.setVisibility(View.VISIBLE);
    }

    private void showMainView() {
        mMainView.setVisibility(View.VISIBLE);
        mLoadingIndicator.setVisibility(View.GONE);
        mBookListEmptyMessage.setVisibility(View.GONE);
    }

}
