package com.uguzon.bibliothque.views.adapters;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.RecyclerView;

import com.uguzon.bibliothque.interactors.CartInteractor;
import com.uguzon.bibliothque.views.models.book.Book;

import java.util.List;

public abstract class BookAdapter<T extends RecyclerView.ViewHolder> extends RecyclerView.Adapter<T> {

    AppCompatActivity mActivity;
    Context mContext;
    List<Book> mBooks;
    CartInteractor mCartInteractor;

    BookAdapter(AppCompatActivity activity) {
        super();
        mActivity = activity;
    }
    @Override
    public int getItemCount() {
        return mBooks != null ? mBooks.size() : 0;
    }

    public void setBooks(List<Book> books) {
        mBooks = books;
        notifyDataSetChanged();
    }

    public void setCartInteractor(CartInteractor cartInteractor) {
        mCartInteractor = cartInteractor;
    }


}
