package com.uguzon.bibliothque.views.models.offer;

import com.uguzon.bibliothque.util.Util;
import com.uguzon.bibliothque.views.models.book.Book;
import com.uguzon.bibliothque.views.models.offer.exceptions.MalformedJsonOffer;

import java.math.BigDecimal;
import java.util.List;

public class OfferMinus extends Offer{

    private BigDecimal mValue;

    public OfferMinus(List<Book> bookList, int value) throws MalformedJsonOffer {
        super(bookList);
        mValue = Util.getBigDecimal(value);
    }

    @Override
    public BigDecimal getTotalPriceWithDiscount() {
        return getTotalPrice().subtract(mValue);
    }
}
