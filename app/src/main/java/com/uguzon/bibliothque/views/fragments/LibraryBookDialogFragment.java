package com.uguzon.bibliothque.views.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.squareup.picasso.Picasso;
import com.uguzon.bibliothque.views.activities.CartActivity;
import com.uguzon.bibliothque.R;
import com.uguzon.bibliothque.views.models.book.Book;

public class LibraryBookDialogFragment extends BookDialogFragment {

    private static final String TAG = LibraryBookDialogFragment.class.getSimpleName();

    public LibraryBookDialogFragment() {
        super();
    }

    @Override
    public void initView() {
        Bundle args = getArguments();
        if (args != null) {
            if (args.containsKey(KEY_BOOK)) {
                try {
                    final Context context = getContext();
                    Book book = (Book) args.getSerializable(KEY_BOOK);
                    mDialogTitleView.setText(R.string.alert_dialog_title);
                    Picasso.with(context)
                            .load(book.getCoverUrl())
                            .into(mCoverView);
                    mTitleView.setText(book.getTitle());
                    mPriceView.setText(book.getStringPrice(getContext()));
                    mAction1Btn.setText(R.string.dialog_back_to_list);
                    mAction1Btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dismiss();
                        }
                    });

                    mAction2Btn.setText(R.string.alert_dialog_to_cart);
                    mAction2Btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            Intent intent = new Intent(context, CartActivity.class);
                            context.startActivity(intent);
                        }
                    });
                } catch (Exception e) {
                    Log.e(TAG, "Echec de la désérialisation du livre");
                }
            }
        }
    }

    public static LibraryBookDialogFragment createInstance(Book book) {
        LibraryBookDialogFragment newInstance = new LibraryBookDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(KEY_BOOK, book);
        newInstance.setArguments(bundle);
        return newInstance;
    }

}
