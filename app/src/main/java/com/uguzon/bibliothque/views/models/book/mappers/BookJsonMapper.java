package com.uguzon.bibliothque.views.models.book.mappers;

import com.uguzon.bibliothque.views.models.book.Book;
import com.uguzon.bibliothque.views.models.book.exceptions.MalformedJsonBook;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class BookJsonMapper {

    private static final String BOOK_JSON_ISBN = "isbn";
    private static final String BOOK_JSON_TITLE = "title";
    private static final String BOOK_JSON_PRICE = "price";
    private static final String BOOK_JSON_COVER = "cover";
    private static final String BOOK_JSON_SYNOPSIS = "synopsis";

    public Book createBook(JSONObject jsonBook) throws MalformedJsonBook {
        try {
            String isbn = jsonBook.getString(BookJsonMapper.BOOK_JSON_ISBN);
            String title = jsonBook.getString(BookJsonMapper.BOOK_JSON_TITLE);
            String coverUrl = jsonBook.getString(BookJsonMapper.BOOK_JSON_COVER);
            int price = jsonBook.getInt(BookJsonMapper.BOOK_JSON_PRICE);
            JSONArray jsonSynopsis = jsonBook.getJSONArray(BookJsonMapper.BOOK_JSON_SYNOPSIS);
            String[] synopsis = new String[jsonSynopsis.length()];
            for (int i=0 ; i<jsonSynopsis.length() ; i++) {
                synopsis[i] = jsonSynopsis.getString(i);
            }
            return new Book(isbn, title, coverUrl, price, synopsis);
        } catch (JSONException e){
            e.printStackTrace();
            throw new MalformedJsonBook(jsonBook);
        }

    }

    public List<Book> createBookList(JSONArray jsonArray) {
        List<Book> bookList = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                bookList.add(createBook(jsonArray.getJSONObject(i)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return bookList;
    }
}
