package com.uguzon.bibliothque.views.models.offer;

import java.util.Collections;
import java.util.List;

public class OfferHelper {

    public static Offer getBestOffer(List<Offer> discountList) {
        return Collections.min(discountList);
    }

}
