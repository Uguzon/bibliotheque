package com.uguzon.bibliothque.views.models.book.exceptions;

import org.json.JSONException;
import org.json.JSONObject;

public class MalformedJsonBook extends JSONException {

    public MalformedJsonBook() {
        super("Enable to parse json book ");
    }

    public MalformedJsonBook(JSONObject jsonObject) {
        super("Enable to parse json book " + jsonObject.toString());
    }
}
