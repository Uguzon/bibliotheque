package com.uguzon.bibliothque.views.models.offer;

import com.uguzon.bibliothque.util.Util;
import com.uguzon.bibliothque.views.models.book.Book;
import com.uguzon.bibliothque.views.models.offer.exceptions.MalformedJsonOffer;

import java.math.BigDecimal;
import java.util.List;

public class OfferSlice extends Offer {

    private int mValue;
    private int mSliceValue;

    public OfferSlice(List<Book> bookList, int value, int sliceValue) throws MalformedJsonOffer {
        super(bookList);
        mValue = value;
        mSliceValue = sliceValue;
    }

    @Override
    public BigDecimal getTotalPriceWithDiscount() {
        try {
            int slices = getTotalPrice().divide(new BigDecimal(mSliceValue)).setScale(0, BigDecimal.ROUND_DOWN).intValue();
            return getTotalPrice().subtract(Util.getBigDecimal(slices * mValue));
        } catch (ArithmeticException e) {
            return getTotalPrice();
        }
    }

}
