package com.uguzon.bibliothque.views.models.offer.exceptions;

import android.content.Context;
import android.content.res.Resources;

import com.uguzon.bibliothque.R;

import org.json.JSONException;
import org.json.JSONObject;

public class MalformedJsonOffer extends JSONException {

    public MalformedJsonOffer() {
        super("Enable to parse json offer ");
    }

    public MalformedJsonOffer(JSONObject jsonObject) {
        super("Enable to parse json offer " + jsonObject.toString());
    }
}
