package com.uguzon.bibliothque.views.fragments;

import android.app.Dialog;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.uguzon.bibliothque.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public abstract class BookDialogFragment extends DialogFragment {

    static final String KEY_BOOK = "keyBook";

    @BindView(R.id.tv_message_title) TextView mDialogTitleView;
    @BindView(R.id.iv_cover) ImageView mCoverView;
    @BindView(R.id.tv_title) TextView mTitleView;
    @BindView(R.id.tv_price) TextView mPriceView;
    @BindView(R.id.btn_action1) Button mAction1Btn;
    @BindView(R.id.btn_action2) Button mAction2Btn;
    @BindView(R.id.tv_synopsis) TextView mSynopsisView;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = LayoutInflater.from(getActivity());
        final View dialogView = inflater.inflate(R.layout.book_dialog, null);
        ButterKnife.bind(this, dialogView);

        initView();

        builder.setView(dialogView);
        return builder.create();
    }

    public abstract void initView();

}
