package com.uguzon.bibliothque.views.models.book;

import android.arch.persistence.room.ColumnInfo;
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.Ignore;
import android.arch.persistence.room.PrimaryKey;
import android.content.Context;
import android.support.annotation.NonNull;

import com.uguzon.bibliothque.util.Util;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;

@Entity
public class Book implements Serializable {

    private static final String TAG = Book.class.getSimpleName();

    @PrimaryKey
    @NonNull
    private String isbn;
    private String title;
    @ColumnInfo(name = "cover_url")
    private String coverUrl;
    private int price;
    private String[] synopsis;

    @Ignore
    public Book() {
    }

    public Book(String isbn, String title, String coverUrl, int price, String[] synopsis) {
        this.isbn = isbn;
        this.title = title;
        this.coverUrl = coverUrl;
        this.price = price;
        this.synopsis = synopsis;
    }

    public String getIsbn() {
        return isbn;
    }

    public void setIsbn(String mIsbn) {
        this.isbn = mIsbn;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getCoverUrl() {
        return coverUrl;
    }

    public void setCoverUrl(String coverUrl) {
        this.coverUrl = coverUrl;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int mPrice) {
        this.price = mPrice;
    }

    public String[] getSynopsis() {
        return synopsis;
    }

    public void setSynopsis(String[] mSynopsis) {
        this.synopsis = mSynopsis;
    }

    public String getLongSynopsis() {
        StringBuilder longSynopsis = new StringBuilder();
        for (String paragraph : synopsis) {
            if (longSynopsis.length() > 0) {
                longSynopsis.append("\n");
            }
            longSynopsis.append("\t" + paragraph);
        }
        return longSynopsis.toString();
    }

    public String getStringPrice(Context context) {
        return Util.getStringPrice(context, this);
    }

    public static boolean inList(List<Book> bookList, Book theBook) {
        for (Book book : bookList) {
            if (book.isbn.equals(theBook.isbn)) {
                return true;
            }
        }
        return false;
    }
}
