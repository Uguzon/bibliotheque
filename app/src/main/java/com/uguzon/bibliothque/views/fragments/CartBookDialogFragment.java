package com.uguzon.bibliothque.views.fragments;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.View;

import com.squareup.picasso.Picasso;
import com.uguzon.bibliothque.R;
import com.uguzon.bibliothque.views.models.book.Book;

public class CartBookDialogFragment extends BookDialogFragment {

    private static final String TAG = CartBookDialogFragment.class.getSimpleName();

    public interface CartBookDialogListener {
        void onRemoveFromCart(CartBookDialogFragment dialog, Book book);
    }

    CartBookDialogListener mListener;

    public CartBookDialogFragment() {
        super();
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        try {
            mListener = (CartBookDialogListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString()
                    + " must implement CartBookDialogListener");
        }
    }

    @Override
    public void initView() {
        Bundle args = getArguments();
        if (args != null) {
            if (args.containsKey(KEY_BOOK)) {
                try {
                    final Context context = getContext();
                    final Book book = (Book) args.getSerializable(KEY_BOOK);

                    mDialogTitleView.setVisibility(View.GONE);
                    Picasso.with(context)
                            .load(book.getCoverUrl())
                            .into(mCoverView);
                    mTitleView.setText(book.getTitle());
                    mPriceView.setText(book.getStringPrice(context));
                    mAction1Btn.setText(R.string.dialog_back_to_cart);
                    mAction1Btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            dismiss();
                        }
                    });

                    mAction2Btn.setText(R.string.btn_remove_from_cart);
                    mAction2Btn.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            mListener.onRemoveFromCart(CartBookDialogFragment.this, book);
                            dismiss();

                        }
                    });
                    mSynopsisView.setText(book.getLongSynopsis());
                } catch (Exception e) {
                    Log.e(TAG, "Echec de la désérialisation du livre");
                }
            }
        }
    }

    public static CartBookDialogFragment createInstance(Book book) {
        CartBookDialogFragment newInstance = new CartBookDialogFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(KEY_BOOK, book);
        newInstance.setArguments(bundle);
        return newInstance;
    }

}
