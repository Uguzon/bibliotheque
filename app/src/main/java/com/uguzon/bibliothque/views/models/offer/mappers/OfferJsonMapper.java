package com.uguzon.bibliothque.views.models.offer.mappers;

import android.util.Log;

import com.uguzon.bibliothque.views.models.book.Book;
import com.uguzon.bibliothque.views.models.offer.Offer;
import com.uguzon.bibliothque.views.models.offer.OfferMinus;
import com.uguzon.bibliothque.views.models.offer.OfferPercentage;
import com.uguzon.bibliothque.views.models.offer.OfferSlice;
import com.uguzon.bibliothque.views.models.offer.exceptions.MalformedJsonOffer;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class OfferJsonMapper {

    private static final String TAG = OfferJsonMapper.class.getSimpleName();

    private static final String OFFER_JSON_TYPE = "type";
    private static final String OFFER_JSON_VALUE = "value";
    private static final String OFFER_JSON_PERCENTAGE = "percentage";
    private static final String OFFER_JSON_MINUS = "minus";
    private static final String OFFER_JSON_SLICE = "slice";
    private static final String OFFER_JSON_SLICE_VALUE = "sliceValue";

    public Offer createOffer(List<Book> bookList, JSONObject jsonObject) throws MalformedJsonOffer {
        try {
            String type = jsonObject.getString(OFFER_JSON_TYPE);
            switch (type) {
                case OFFER_JSON_PERCENTAGE :
                    return new OfferPercentage(bookList, jsonObject.getInt(OFFER_JSON_VALUE));
                case OFFER_JSON_MINUS :
                    return new OfferMinus(bookList, jsonObject.getInt(OFFER_JSON_VALUE));
                case OFFER_JSON_SLICE :
                    return new OfferSlice(bookList, jsonObject.getInt(OFFER_JSON_VALUE), jsonObject.getInt(OFFER_JSON_SLICE_VALUE));
                default :
                    throw new MalformedJsonOffer(jsonObject);
            }

        } catch (JSONException e) {
            throw new MalformedJsonOffer(jsonObject);
        }
    }

    public List<Offer> createOfferList(List<Book> bookList, JSONArray jsonArray) {
        List<Offer> offerList = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++) {
            try {
                offerList.add(new OfferJsonMapper().createOffer(bookList, jsonArray.getJSONObject(i)));
                Log.v(TAG, "Offer " + i + " added");
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return offerList;

    }
}
