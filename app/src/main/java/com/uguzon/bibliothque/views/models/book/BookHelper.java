package com.uguzon.bibliothque.views.models.book;

import com.uguzon.bibliothque.util.Util;

import java.math.BigDecimal;
import java.util.List;

public class BookHelper {

    public static BigDecimal getTotalPrice(List<Book> bookList) {
        int total = 0;
        for (Book book : bookList) {
            total += book.getPrice();
        }
        return Util.getBigDecimal(total);
    }
}
