package com.uguzon.bibliothque.views.models.offer;

import com.uguzon.bibliothque.util.Util;
import com.uguzon.bibliothque.views.models.book.Book;
import com.uguzon.bibliothque.views.models.offer.exceptions.MalformedJsonOffer;

import java.math.BigDecimal;
import java.util.List;

public class OfferPercentage extends Offer{

    private int mValue;

    public OfferPercentage(List<Book> bookList, int value) throws MalformedJsonOffer {
        super(bookList);
        mValue = value;
        if (mValue < 0 || mValue > 100) {
            throw new MalformedJsonOffer();
        }
    }

    @Override
    public BigDecimal getTotalPriceWithDiscount() {
        BigDecimal multiplicant = Util.getBigDecimal((100 - mValue) / 100.0);
        return Util.getBigDecimal(getTotalPrice().multiply(multiplicant));
    }
}
