package com.uguzon.bibliothque.views.activities;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.uguzon.bibliothque.R;
import com.uguzon.bibliothque.interactors.CartInteractor;
import com.uguzon.bibliothque.views.adapters.LibraryBookAdapter;
import com.uguzon.bibliothque.repositories.room_database.AppDatabase;
import com.uguzon.bibliothque.interactors.CartRoomInteractor;
import com.uguzon.bibliothque.views.models.book.Book;
import com.uguzon.bibliothque.interactors.LibraryContentWebInteractor;
import com.uguzon.bibliothque.viewModels.LibraryViewModel;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class LibraryActivity extends AppCompatActivity {

    private static final String TAG = LibraryActivity.class.getSimpleName();

    private LibraryBookAdapter mBookAdapter;

    @BindView(R.id.recyclerview_books) RecyclerView mRecyclerView;
    @BindView(R.id.pb_loading_indicator) ProgressBar mLoadingIndicator;
    @BindView(R.id.tv_message_empty_book_list) TextView mBookListEmptyMessage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_library);
        ButterKnife.bind(this);

        CartInteractor cartInteractor = new CartRoomInteractor(AppDatabase.getDataBaseInstance(this));
        mBookAdapter = new LibraryBookAdapter(this);
        mBookAdapter.setCartInteractor(cartInteractor);

        LibraryViewModel viewModel = ViewModelProviders.of(this).get(LibraryViewModel.class);
        viewModel.init(cartInteractor, new LibraryContentWebInteractor(this));
        viewModel.getBookList().observe(this, new Observer<List<Book>>() {
            @Override
            public void onChanged(@Nullable List<Book> books) {
                mBookAdapter.setBooks(books);
                if (books == null || books.size() == 0) {
                    showBookListEmptyMessage();
                } else {
                    showMainView();
                }

            }
        });

        viewModel.getCartList().observe(this, new Observer<List<Book>>() {
            @Override
            public void onChanged(@Nullable List<Book> books) {
                mBookAdapter.setCartList(books);
            }
        });

        mRecyclerView.setAdapter(mBookAdapter);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        mRecyclerView.setLayoutManager(layoutManager);

        showLoadingIndicator();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_menu, menu);
        for (int i = 0; i < menu.size(); i++) {
            if (menu.getItem(i).getItemId() != R.id.action_cart) {
                menu.getItem(i).setVisible(false);
            }
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.action_cart) {
            startActivity(new Intent(this, CartActivity.class));
        }
        return super.onOptionsItemSelected(item);
    }

    private void showLoadingIndicator() {
        mRecyclerView.setVisibility(View.GONE);
        mLoadingIndicator.setVisibility(View.VISIBLE);
        mBookListEmptyMessage.setVisibility(View.GONE);
    }

    private void showBookListEmptyMessage() {
        mRecyclerView.setVisibility(View.GONE);
        mLoadingIndicator.setVisibility(View.GONE);
        mBookListEmptyMessage.setVisibility(View.VISIBLE);
    }

    private void showMainView() {
        mRecyclerView.setVisibility(View.VISIBLE);
        mLoadingIndicator.setVisibility(View.GONE);
        mBookListEmptyMessage.setVisibility(View.GONE);
    }

}
