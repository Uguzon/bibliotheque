package com.uguzon.bibliothque.views.models.offer;

import android.support.annotation.NonNull;

import com.uguzon.bibliothque.views.models.book.Book;
import com.uguzon.bibliothque.views.models.book.BookHelper;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.List;

public abstract class Offer implements Comparable<Offer>{

    private List<Book> mBookList;

    public Offer(List<Book> bookList) {
        mBookList = bookList;
    }

    public abstract BigDecimal getTotalPriceWithDiscount();

    public BigDecimal getTotalPrice() {
        return BookHelper.getTotalPrice(mBookList);
    }

    @Override
    public int compareTo(@NonNull Offer o) {
        return (this.getTotalPriceWithDiscount().subtract(o.getTotalPriceWithDiscount()).setScale(0, RoundingMode.FLOOR).intValue());
    }

}
